$(window).scroll(function() {
	scrollTop = $(window).scrollTop(),
	divOffset = $('#boxgallery').offset().top,
	dist = (divOffset - scrollTop);
	if (dist <= -100) {
		$('.menu').addClass("fix_menu");
		$('.logo').addClass('fix_logo');
	} else {
		$('.menu').removeClass("fix_menu");
		$('.logo').removeClass('fix_logo');
	}
});
// Menu Scripts
$(document).ready(function(){
	$('.main_menu li').hover(function(){
		$('#sub_menu').toggleClass('in out');
	});
});

// small slider
$(document).ready(function() {
    $("#lightSlider").lightSlider({
        item: 3,
        autoWidth: false,
        slideMove: 1, // slidemove will be 1 if loop is true
        slideMargin: 10,
 
        addClass: '',
        mode: "slide",
        useCSS: true,
        cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
        easing: 'linear', //'for jquery animation',////
 
        speed: 1000, //ms'
        auto: true,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,
 
        keyPress: false,
        controls: true,
        prevHtml: '',
        nextHtml: '',
 
        rtl:false,
        adaptiveHeight:false,
 
        vertical:false,
        verticalHeight:500,
        vThumbWidth:100,
 
        thumbItem:10,
        pager: true,
        gallery: false,
        galleryMargin: 5,
        thumbMargin: 5,
        currentPagerPosition: 'middle',
 
        enableTouch:true,
        enableDrag:true,
        freeMove:true,
        swipeThreshold: 40,
 
        responsive : [],
 
        onBeforeStart: function (el) {},
        onSliderLoad: function (el) {},
        onBeforeSlide: function (el) {},
        onAfterSlide: function (el) {},
        onBeforeNextSlide: function (el) {},
        onBeforePrevSlide: function (el) {}
    });
});

// veiw port

jQuery(document).ready(function() {
    jQuery('.post').addClass("ani_hidden").viewportChecker({
        classToAdd: 'ani_visible animated fadeInUp',
        offset: 100
       });
});