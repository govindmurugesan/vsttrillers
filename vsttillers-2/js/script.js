$(window).scroll(function() {
	scrollTop = $(window).scrollTop(),
	divOffset = $('.wrapper_sec').offset().top,
	dist = (divOffset - scrollTop);
	if (dist <= 600) {
		$('.menu').addClass("fix_menu");
		$('.logo').addClass('fix_logo');
	} else {
		$('.menu').removeClass("fix_menu");
		$('.logo').removeClass('fix_logo');
	}
});
// Menu Scripts
$(document).ready(function(){
	$('.main_menu li').hover(function(){
		$('#sub_menu').toggleClass('in out');
	});
});
// veiw port

jQuery(document).ready(function() {
    jQuery('.post').addClass("ani_hidden").viewportChecker({
        classToAdd: 'ani_visible animated fadeInUp',
        offset: 100
       });
});